Feature: Login nih bosss

  Scenario: user want to login with correct credentials
    Given user clicks entrypoint button
    When user inputs "bomxyz" as username
    And user inputs "stockbit" as password
    And user clicks login button

  Scenario: user want to login with invalid username
    Given user clicks entrypoint button
    When user inputs "durant09" as username
    And user inputs "stockbittt" as password
    And user clicks login button
    Then user will see "Username atau password salah. Mohon coba lagi." as an error message

  Scenario: user want to login with invalid password
    Given user clicks entrypoint button
    When user inputs "bomxyz" as username
    And user inputs "stockbittt" as password
    And user clicks login button
    Then user will see "Username atau password salah. Mohon coba lagi." as an error message

  Scenario: user want to login with blanks credentials
    Given user clicks entrypoint button
    When user inputs "" as username
    And user inputs "" as password
    And user clicks login button
    Then user will see "Mohon masukkan username/email dan password kamu." as an error message
