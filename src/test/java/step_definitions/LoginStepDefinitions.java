package step_definitions;

import io.cucumber.java8.En;
import com.stockbit.page_object.LoginPage;
public class LoginStepDefinitions implements En{

    LoginPage lp = new LoginPage();
    public LoginStepDefinitions() {
        Given("^user clicks entrypoint button$", () -> {
            lp.clickLoginEntry();
        });
        When("^user inputs \"([^\"]*)\" as username$", (String username) -> {
            lp.inputUsername(username);
        });
        And("^user inputs \"([^\"]*)\" as password$", (String password) -> {
            lp.inputPassword(password);
        });
        And("^user clicks login button$", () -> {
            lp.clickButtonLogin();
        });
        Then("^user will see \"([^\"]*)\" as an error message$", (String message) -> {
            lp.checkLoginErrorMessage(message);
        });


    }
}
