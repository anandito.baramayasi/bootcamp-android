package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import com.stockbit.android_driver.AndroidDriverInstance;

import static com.stockbit.utils.Constants.ELEMENTS;
import static com.stockbit.utils.Utils.loadElementProperties;
public class AndroidDriverHooks {
    @Before
    public void initializeDriver(){
        AndroidDriverInstance.initialize();
        loadElementProperties(ELEMENTS);
    }

    @After
    public void quitDriver(){
        AndroidDriverInstance.quit();
    }
}
