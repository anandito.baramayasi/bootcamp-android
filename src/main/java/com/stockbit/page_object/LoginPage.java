package com.stockbit.page_object;

import com.stockbit.base.BasePageObj;

import java.io.IOException;

public class LoginPage extends BasePageObj{
    public void clickLoginEntry(){
        isDisplayed("COMPANY_LOGO");
        tap("LOGIN_TOOLBAR_TITLE");
    }

    public void inputUsername(String username) {
        InputText("TEXTFIELD_USERNAME", username);
    }

    public void inputPassword(String password) {
        InputText("TEXTFIELD_PASSWORD",password);
    }

    public void clickButtonLogin(){
        tap("BUTTON_LOGIN_MASUK");
    }
    public void checkLoginErrorMessage(String value){
        ValueCheck("TOAST_MESSAGE_LOGIN",value);
    }

}
