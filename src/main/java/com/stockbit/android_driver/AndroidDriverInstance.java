package com.stockbit.android_driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.remote.DesiredCapabilities;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AndroidDriverInstance {

    static Dotenv env = Dotenv.load();
    public static AndroidDriver<AndroidElement> androidDriver;

    public static void initialize() {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName","Android");
        dc.setCapability("platformVersion","13.0");
        dc.setCapability("deviceName",env.get("android_mufti"));
        dc.setCapability("app",env.get("mufti_apk"));
        dc.setCapability("appWaitActivity", "*");
        dc.setCapability("autoGrantPermissions","true");

        try{
            androidDriver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            androidDriver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        }
        catch(MalformedURLException e){
            e.printStackTrace();
        }

    }

    public static void quit(){
        androidDriver.quit();
    }
}
