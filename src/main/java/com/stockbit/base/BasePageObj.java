package com.stockbit.base;

import com.stockbit.utils.Constants;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.By;
import com.stockbit.android_driver.AndroidDriverInstance;
import org.openqa.selenium.NoSuchElementException;
import com.stockbit.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class BasePageObj {

    Dotenv env = Dotenv.load();

    public AndroidDriver driver(){
        return AndroidDriverInstance.androidDriver;
    }

    public  AndroidElement waitStrats(ExpectedCondition<WebElement> conditions){
        WebDriverWait wait = new WebDriverWait(driver(), Constants.TIMEOUT);
        return (AndroidElement) wait.until(conditions);
    }

    public AndroidElement waitTillClickable(By element){
        return waitStrats(ExpectedConditions.elementToBeClickable(element));
    }
    public By element(String elementLocator){
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);

        if(elementValue == null){
            throw new NoSuchElementException("no such element at : " + elementLocator);
        } else {
            String[] locator = elementValue.split("_");
            String locatorType = locator[0];
            String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);
            switch (locatorType){
                case "id":
                    if(Objects.equals(env.get("ENV"), "prod")){
                        return By.id("com.stockbit.android:id/" + locatorValue);
                    } else {
                        return By.id("com.stockbitdev.android:id/" + locatorValue);
                    }
                case "xpath":
                    return By.xpath(locatorValue);
                case "accessibilityId":
                    return MobileBy.AccessibilityId(elementLocator);
                default:
                    throw new IllegalStateException("Unexpected Value: " + locatorType);
            }
        }
    }

    public void InputText(String locator, String text){
        waitTillClickable(element(locator)).sendKeys(text);
    }


    public void tap(String locator){
        waitTillClickable(element(locator)).click();
    }
    public void isDisplayed(String locator){
        driver().findElement(element(locator)).isDisplayed();
    }
    public boolean Checker (String locator){
        MobileElement object = (MobileElement) driver().findElement(element(locator));
        try{object.isDisplayed();}
        catch(NoSuchElementException e){
            return false;
        }
        return true;
    }

    public void ValueCheck(String locator, String value){
        MobileElement object = (MobileElement) driver().findElement(element(locator));
        System.out.println(Checker(element(locator).toString()));
        if(Checker(element(locator).toString())){
            String attribute = object.getAttribute("text");
            if(!attribute.equals(value)){
                throw new AssertionError("Value is not equals to value parsed from element");
            }
        }
        else {
            throw new org.openqa.selenium.NoSuchElementException("No element was found");
        }

    }
}
