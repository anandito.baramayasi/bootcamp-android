## Android-bootcamp ##

**Automation for testing stockbit staging apps on Android OS**

Tools used:

JAVA SDK18(correto 18) : https://www.oracle.com/java/technologies/downloads/

Gradle 6.9.1

Appium 1.22.3

Android SDK: https://developer.android.com/studio

Dependencies used (copy and paste this into Dependencies{} of build.gradle ):
	
    testImplementation 'org.junit.jupiter:junit-jupiter:5.8.2'
    testImplementation 'junit:junit:4.13.2'
    testRuntimeOnly 'org.junit.vintage:junit-vintage-engine:5.8.2'
    testCompile 'org.junit.jupiter:junit-jupiter-api:5.8.2'
    compile 'org.seleniumhq.selenium:selenium-java:3.141.59'
    compile 'io.cucumber:cucumber-java:7.12.0'
    compile 'io.cucumber:cucumber-junit-platform-engine:7.12.0'
    testCompile 'io.cucumber:cucumber-junit:7.12.0'
    compile 'io.appium:java-client:7.5.1'
    implementation 'io.github.cdimascio:java-dotenv:5.2.2'
    implementation 'org.apache.logging.log4j:log4j-core:2.19.0'
    /* Using cucumber-java8 */
    testImplementation 'junit:junit:4.13.2'
    testImplementation 'io.cucumber:cucumber-java8:7.12.0'
    testImplementation 'io.cucumber:cucumber-junit:7.12.0'
    implementation 'com.google.code.gson:gson:2.10.1'
